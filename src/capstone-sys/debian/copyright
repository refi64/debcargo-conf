Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: capstone-sys
Upstream-Contact:
 m4b <m4b.github.io@gmail.com>
 tmfink <tmfinken@gmail.com>
Source: https://github.com/capstone-rust/capstone-rs/tree/master/capstone-sys

Files: *
Copyright:
 2015-2017 Richo Healey <richo@psych0tik.net>
 2016-2017 m4b <m4b.github.io@gmail.com>
 2017-2020 Travis Finkenauer <tmfinken@gmail.com>
 2013, COSEINC
 1998-2001 Karl Stenerud.  All rights reserved.
License: MIT

Files: capstone/MCFixedLenDisassembler.h
       capstone/MCInst.h
       capstone/MCInstrDesc.h
       capstone/MCRegisterInfo.c
       capstone/MCRegisterInfo.h
       capstone/MathExtras.h
       capstone/LEB128.h
       capstone/arch/AArch64/AArch64AddressingModes.h
       capstone/arch/AArch64/AArch64BaseInfo.c
       capstone/arch/AArch64/AArch64BaseInfo.h
       capstone/arch/AArch64/AArch64Disassembler.c
       capstone/arch/AArch64/AArch64InstPrinter.c
       capstone/arch/AArch64/AArch64InstPrinter.h
       capstone/arch/ARM/ARMAddressingModes.h
       capstone/arch/ARM/ARMBaseInfo.h
       capstone/arch/ARM/ARMDisassembler.c
       capstone/arch/ARM/ARMInstPrinter.c
       capstone/arch/ARM/ARMInstPrinter.h
       capstone/arch/Mips/MipsDisassembler.c
       capstone/arch/Mips/MipsInstPrinter.c
       capstone/arch/Mips/MipsInstPrinter.h
       capstone/arch/PowerPC/PPCDisassembler.c
       capstone/arch/PowerPC/PPCInstPrinter.c
       capstone/arch/PowerPC/PPCPredicates.h
       capstone/arch/Sparc/Sparc.h
       capstone/arch/Sparc/SparcDisassembler.c
       capstone/arch/Sparc/SparcInstPrinter.c
       capstone/arch/SystemZ/SystemZDisassembler.c
       capstone/arch/SystemZ/SystemZInstPrinter.c
       capstone/arch/SystemZ/SystemZMCTargetDesc.c
       capstone/arch/SystemZ/SystemZMCTargetDesc.h
       capstone/arch/X86/X86BaseInfo.h
       capstone/arch/X86/X86Disassembler.h
       capstone/arch/X86/X86DisassemblerDecoder.h
       capstone/arch/X86/X86DisassemblerDecoderCommon.h
       capstone/arch/X86/X86InstPrinter.h
       capstone/arch/X86/X86ATTInstPrinter.c
       capstone/arch/X86/X86Disassembler.c
       capstone/arch/X86/X86DisassemblerDecoder.c
       capstone/arch/X86/X86IntelInstPrinter.c
       capstone/arch/X86/X86Mapping.c
       capstone/arch/XCore/XCoreDisassembler.c
       capstone/arch/XCore/XCoreInstPrinter.c
Copyright: (c) 2003-2013 University of Illinois at Urbana-Champaign.
License: BSD-3-clause and MIT

Files: debian/*
Copyright:
 2020-2021 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2020-2021 Michael R. Crusoe <crusoe@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: BSD-3-clause
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal with
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
     * Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimers.
 .
     * Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimers in the
       documentation and/or other materials provided with the distribution.
 .
     * Neither the names of the LLVM Team, University of Illinois at
       Urbana-Champaign, nor the names of its contributors may be used to
       endorse or promote products derived from this Software without specific
       prior written permission.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE
 SOFTWARE.

License: BSD-4-Clause-UC
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. All advertising materials mentioning features or use of this software
    must display the following acknowledgement:
      This product includes software developed by the University of
      California, Berkeley and its contributors.
 4. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

