Source: rust-buffered-reader
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-bzip2-0.4+default-dev <!nocheck>,
 librust-flate2-1+default-dev (>= 1.0.1-~~) <!nocheck>,
 librust-libc-0.2+default-dev (>= 0.2.66-~~) <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/buffered-reader]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/buffered-reader
Homepage: https://sequoia-pgp.org/
Rules-Requires-Root: no

Package: librust-buffered-reader-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-libc-0.2+default-dev (>= 0.2.66-~~)
Recommends:
 librust-buffered-reader+compression-dev (= ${binary:Version})
Suggests:
 librust-buffered-reader+bzip2-dev (= ${binary:Version}),
 librust-buffered-reader+flate2-dev (= ${binary:Version})
Provides:
 librust-buffered-reader-1-dev (= ${binary:Version}),
 librust-buffered-reader-1.1-dev (= ${binary:Version}),
 librust-buffered-reader-1.1.1-dev (= ${binary:Version})
Description: Super-powered Reader - Rust source code
 This package contains the source for the Rust buffered-reader crate, packaged
 by debcargo for use with cargo and dh-cargo.

Package: librust-buffered-reader+bzip2-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-buffered-reader-dev (= ${binary:Version}),
 librust-bzip2-0.4+default-dev
Provides:
 librust-buffered-reader+compression-bzip2-dev (= ${binary:Version}),
 librust-buffered-reader-1+bzip2-dev (= ${binary:Version}),
 librust-buffered-reader-1+compression-bzip2-dev (= ${binary:Version}),
 librust-buffered-reader-1.1+bzip2-dev (= ${binary:Version}),
 librust-buffered-reader-1.1+compression-bzip2-dev (= ${binary:Version}),
 librust-buffered-reader-1.1.1+bzip2-dev (= ${binary:Version}),
 librust-buffered-reader-1.1.1+compression-bzip2-dev (= ${binary:Version})
Description: Super-powered Reader - feature "bzip2" and 1 more
 This metapackage enables feature "bzip2" for the Rust buffered-reader crate, by
 pulling in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "compression-bzip2" feature.

Package: librust-buffered-reader+compression-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-buffered-reader-dev (= ${binary:Version}),
 librust-buffered-reader+compression-deflate-dev (= ${binary:Version}),
 librust-buffered-reader+compression-bzip2-dev (= ${binary:Version})
Provides:
 librust-buffered-reader+default-dev (= ${binary:Version}),
 librust-buffered-reader-1+compression-dev (= ${binary:Version}),
 librust-buffered-reader-1+default-dev (= ${binary:Version}),
 librust-buffered-reader-1.1+compression-dev (= ${binary:Version}),
 librust-buffered-reader-1.1+default-dev (= ${binary:Version}),
 librust-buffered-reader-1.1.1+compression-dev (= ${binary:Version}),
 librust-buffered-reader-1.1.1+default-dev (= ${binary:Version})
Description: Super-powered Reader - feature "compression" and 1 more
 This metapackage enables feature "compression" for the Rust buffered-reader
 crate, by pulling in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "default" feature.

Package: librust-buffered-reader+flate2-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-buffered-reader-dev (= ${binary:Version}),
 librust-flate2-1+default-dev (>= 1.0.1-~~)
Provides:
 librust-buffered-reader+compression-deflate-dev (= ${binary:Version}),
 librust-buffered-reader-1+flate2-dev (= ${binary:Version}),
 librust-buffered-reader-1+compression-deflate-dev (= ${binary:Version}),
 librust-buffered-reader-1.1+flate2-dev (= ${binary:Version}),
 librust-buffered-reader-1.1+compression-deflate-dev (= ${binary:Version}),
 librust-buffered-reader-1.1.1+flate2-dev (= ${binary:Version}),
 librust-buffered-reader-1.1.1+compression-deflate-dev (= ${binary:Version})
Description: Super-powered Reader - feature "flate2" and 1 more
 This metapackage enables feature "flate2" for the Rust buffered-reader crate,
 by pulling in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "compression-deflate" feature.
